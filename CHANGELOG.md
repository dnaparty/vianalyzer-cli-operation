# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.1] - 2020-12-1
#### Fix
- fix: test veredict for exceeded failures removed

## [1.0.0] - 2020-12-10
### Added
- Initial Version
- feat: unit unit testing
- feat: included code quality report option
- feat: parse results based on test ranking
- feat: ni package building configuration
